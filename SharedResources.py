from PyQt5.QtGui import QFont
import datetime
import time
import math
import os


'''Globals used throughout the files'''


class ids():
    ID_COUNTER = round(time.time())
    def new():
        ids.ID_COUNTER += 10
        return ids.ID_COUNTER


class path():
    root = os.path.dirname(os.path.realpath(__file__))
    def joinroot(*newpath):
        p = path.root
        for ppath in newpath:
            p = os.path.join(p, ppath)
        return p

class timeU():
    second = 1
    minute = 60*second
    hour = 60*minute
    day = 24*hour
    week = 7*day
    def seconds(mult):
        return timeU.second*mult
    def minutes(mult):
        return timeU.minute*mult
    def hours(mult):
        return timeU.hour*mult
    def days(mult):
        return timeU.day*mult
    def weeks(mult):
        return timeU.week*mult
    
    def secs_to_dhm(secs):
        # print('got ',secs)
        day = secs // (24 * 3600)
        secs = secs % (24 * 3600)
        hour = secs // 3600
        secs %= 3600
        minutes = secs // 60
        # print('returned',day,'days,',hour,'hours, and',minutes,'minutes')
        return day,hour,minutes

class func():
    def delta_details(tdelta):
        d = {}
        d['weeks'], d['days'] = divmod(tdelta.days, 7)
        d["hours"], rem = divmod(tdelta.seconds, 3600)
        d["minutes"], d["seconds"] = divmod(rem, 60)
        # print(d)
        return d
    def ignore(*args,**kwargs):
        print('ignore called with',args,kwargs)

class timings():
    timeupdate = 250

class font():
    name = 'Arial'
    p = QFont(name, 10)
    h3 = QFont(name, 12)
    h2 = QFont(name, 14)
    h1 = QFont(name, 20)


class css():
    graylabel = 'QLabel\n{color: gray;}'
    greenlabel = 'QLabel {color: green;}'
    salmonlabel = 'QLabel {color: salmon;}'
    linestyle = 'border-style: solid;\nborder-width: 10px;\nborder-color: black;'
    empty = ''

# class date():
#     def format_timeleft(timestamp, method='biggest', from_pov=None):
#         pov = time.time() if from_pov == None else from_pov
#         left = timestamp - pov

#         out = {'units':{}, 'combined':{}}
#         out['units']['weeks'] = left / timeU.week
#         out['units']['days'] = left / timeU.day
#         out['units']['minutes'] = left / timeU.minute
#         out['units']['seconds'] = left


#         out['combined']['weeks'] = left / timeU.week
#         out['combined']['days'] = left / timeU.day
#         out['combined']['minutes'] = left / timeU.minute
#         out['combined']['seconds'] = left
#         # out['days'] = (left - round()) % timeU.day
#         # out['minutes'] = left %

#         # return {'weeks':, 'days':, 'minutes':,}
#         return out
# print(date.format_timeleft(time.time()+timeU.week))