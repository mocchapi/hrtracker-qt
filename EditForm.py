import os
import sys
import time
import datetime

from DataClasses import Schedule, MedicineData, RepeatSchedule
from SharedResources import font, timeU, timings
import SharedResources as SR

from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QHBoxLayout, QVBoxLayout
from PyQt5.QtCore import QObject, QTimer, Qt, QTime, QDateTime

class EditForm(QWidget):
    def __init__(self, vault, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vault = vault
        self.initUI()
        self.set_data(MedicineData('', '', '', '', '', '', RepeatSchedule(timeU.hour, time.time())))
        self.callback = SR.func.ignore

    def initUI(self):
        loadUi(SR.path.joinroot('ui','EditForm.ui'), self)
        self.setWindowModality(Qt.ApplicationModal)
        self.BTN_cancel.clicked.connect(self.hide)
        self.BTN_save.clicked.connect(self.save)
        self.Name.textChanged.connect(self.validate)
        self.GraceTime.timeChanged.connect(self.validate)
        self.RepeatDays.valueChanged.connect(self.validate)
        self.RepeatHours.valueChanged.connect(self.validate)
        self.RepeatMinutes.valueChanged.connect(self.validate)
    
    def validate(self,*args, **kwargs):
        repeatmins = ((self.RepeatDays.value() * 24 + self.RepeatHours.value()) * 60 + self.RepeatMinutes.value()) * 60
        grace = self.GraceTime.time().toPyTime()
        grace = (grace.hour * 60 + grace.minute) * 60 + grace.second
        if self.Name.text() == '' or repeatmins == 0 or grace >= repeatmins:
            self.BTN_save.setDisabled(True)
        else:
            self.BTN_save.setDisabled(False)

    def set_data(self, data):
        # self.empty()
        self.data = data
        self.populate()

    def apply(self):
        self.data.dosage = self.Dosage.text()
        self.data.name = self.Name.text()
        self.data.usage = self.Usage.toPlainText()
        self.data.description = self.Description.toPlainText()
        self.data.side_effects = self.SideEffects.toPlainText()
        self.data.missed_dose = self.MissedDose.toPlainText()
        self.data.notes = self.Notes.toPlainText()
        
        # print(self.RepeatTime.dateTime())
        # print(dir(self.RepeatTime.dateTime()))
        # print(self.RepeatTime.dateTime().toPyDateTime())
        # print(dir(self.GraceTime.time()))
        # print(dir(self.GraceTime.time().toPyTime()))
        # breakpoint()
        # newsched = RepeatSchedule(QTime(0, 0, 0).secsTo(self.RepeatTime.time()), QTime(0, 0, 0).secsTo(self.StartTime.time()) + self.StartTime.toTime_t())
        repeat = (self.RepeatDays.value() * 24 + self.RepeatHours.value()) * 60 + self.RepeatMinutes.value()
        repeat *= 60
        
        grace = self.GraceTime.time().toPyTime()
        grace_s = (grace.hour * 60 + grace.minute) * 60 + grace.second
        newsched = RepeatSchedule(
            repeat, 
            self.StartTime.dateTime().toPyDateTime().timestamp(),
            grace_time=grace_s
            )
        # print('all done')
        self.data.set_schedule(newsched)

    
    def save(self):
        self.apply()
        self.vault.add(self.data)
        self.callback(self.data)
        self.hide()

    def showAndEdit(self, data, callback=SR.func.ignore):
        print('called to edit',data.name)        
        self.callback = callback
        self.data = data
        self.populate()
        self.show()
    
    def showAndCreate(self, callback):
        print('called to create')        
        self.empty()
        self.show()
        self.callback = callback

    def populate(self):
        self.Name.setText(self.data.name)
        self.Dosage.setText(self.data.dosage)
        self.Description.setText(self.data.description)
        self.Usage.setText(self.data.usage)
        self.SideEffects.setText(self.data.side_effects)
        self.MissedDose.setText(self.data.missed_dose)
        self.Notes.setText(self.data.notes)
        # self.RepeatTime()
        # self.RepeatTime.setTime(QTime(0,self.data.get_schedule().repeat_time))
        days,hours,minutes = SR.timeU.secs_to_dhm(self.data.get_schedule().repeat_time)
        self.RepeatDays.setValue(days)
        self.RepeatHours.setValue(hours)
        self.RepeatMinutes.setValue(minutes)
        # print(dir(QDateTime))
        self.StartTime.setDateTime(QDateTime.fromSecsSinceEpoch(int(self.data.get_schedule().start)))
        self.GraceTime.setTime(QTime(0, self.data.get_schedule().grace_time))
        self.validate()

    def empty(self):
        self.data = (MedicineData('', '', '', '', '', '', RepeatSchedule(timeU.hour, time.time())))
        self.populate()