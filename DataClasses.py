import os
import math
import json
import time
from SharedResources import timeU
import SharedResources as SR
from datetime import datetime

class VaultLoadException(Exception):
    pass


class Vault():
    def __init__(self, filepath):
        self.filepath = filepath
        if not os.path.isfile(self.filepath):
            with open(self.filepath, 'w') as f:
                f.write(json.dumps({'medications':{},'meta':{'loads':0, 'dbversion':'1', 'softwareversion':'1', 'dbtimestamp':time.time()}}))
                print('vault',self.filepath,'initialised')
        self.reload()

    def reload(self):
        self.db = {'medications':{}}
        self.data = self.db['medications']
        with open(self.filepath, 'r') as f:
            tmp = f.read()  
        tmp = json.loads(tmp)
        self.db['meta'] = tmp['meta']
        print('vault',self.filepath,'previously updated',self.db['meta']['loads'],'times')
        for item in tmp['medications'].values():
            # print(item)
            self.data[item['id']] = MedicineData.from_dict(item)
            # print(self.data[key].get_schedule())
            print('loaded',item['name'],f'({item["id"]})')


    def idify(self, obj):
        if type(obj) != int:
            return obj.id
        return obj

    def get(self, id):
        return self.data.get(self.idify(id))

    def has(self, id):
        return self.idify(id) in self.data

    def get_all(self):
        return self.data.copy()

    def add(self, obj, id=None):
        id = id if id != None else obj.id
        if self.has(id):
            print('modified medicine in vault:',f'"{obj.name}"','with id',id)
        else:
            print('added new medicine to vault:',f'"{obj.name}"','with id',id)
        self.data[id] = obj

    def remove(self, id):
        id = self.idify(id)
        print(f'removing medicine "{self.data[id]}" ({id}) from the vault')
        del self.data[id]

    def save(self):
        self.db['meta']['loads'] += 1
        out = {}
        out['meta'] = self.db['meta'].copy()
        out['medications'] = {}
        for data in self.data.values():
            out['medications'][data.id] = data.as_saveable()
        jsono = json.dumps(out)
        with open(self.filepath,'w') as f:
            f.write(jsono)
        print('vault saved to',self.filepath)



class Schedule():
    def __init__(self, start, grace_time=timeU.hour):
        self.start = start
        self.grace_time = max(0, grace_time)
        self.half_grace = grace_time
        self.prev = self.start
        self.get_current()
        # print('new schedule made')

    @classmethod
    def from_dict(cls, d):
        return cls(d['start'], d['grace_time'])


    def _get(self, timestamp):
        return 0

    def get_current(self, from_time=None):
        pov = from_time if from_time != None else time.time()
        if (self.prev - self.half_grace) < pov:
            self.prev = self._get(pov)
        return self.prev
    
    def get_current_datetime(self, *args, **kwargs):
        return datetime.fromtimestamp(self.get_current(*args, **kwargs))

    def _dict_(self):
        return {}

    def __dict__(self):
        out = {
            'start': self.start,
            'grace_time': self.grace_time,
        }
        out |= self._dict_()
        return out


class RepeatSchedule(Schedule):
    def __init__(self, time_in_sec, *args, **kwargs):
        self.repeat_time = time_in_sec
        # print('repeat every', self.repeat_time,'seconds')
        # print('            ', self.repeat_time/timeU.minute,'minutes')
        # print('            ', self.repeat_time/timeU.hour,'hours')
        # print('            ', self.repeat_time/timeU.day,'days')
        # print('            ', self.repeat_time/timeU.week,'weeks')
        super().__init__(*args, **kwargs)



    def _get(self, timestamp):
        x = 0
        while True:
            check = ((self.start + self.repeat_time * x) + self.half_grace)
            # print(' did',check, 'at x =',x)
            # print(' which translates to',datetime.fromtimestamp(check) - datetime.now())
            if check > timestamp:
                # print('accepted')
                return self.start + self.repeat_time * x
            x += 1
    
    def _dict_(self):
        return {'repeat_time': self.repeat_time}
    # def closest_to(self)
    @classmethod
    def from_dict(cls, d):
        return cls(d['repeat_time'], d['start'], d['grace_time'])

class MedicineData():
    def __init__(self, name='', description='', missed_dose='', usage='', side_effects='', dosage='', schedule: Schedule=None, notes='', history=None, paused=False, id=None):
        self.id = id if id != None else SR.ids.new()
        self.name = name
        self.missed_dose = missed_dose
        self.description = description
        self.usage = usage
        self.dosage = dosage
        self.side_effects = side_effects
        self.schedule = schedule
    
        self.notes = notes
        self.history = {'categories':{'applied':{}, 'missed':{}, 'resumed medication':{}, 'paused medication':{}, 'changed dose':{}},'global':{}} if history == None else history
        self.paused = paused
        # print('new medicine with ID',self.id,'named',self.name)

    def setPaused(self, state):
        self.paused = bool(state)
        if self.paused:
            self.log_paused()
        else:
            self.log_resumed()

    @classmethod
    def from_dict(cls, d: dict):
        print(d['schedule'])
        if type(d['schedule']) == dict:
            d['schedule'] = RepeatSchedule.from_dict(d['schedule'])
        return cls(
            d['name'],
            d['description'],
            d['missed_dose'],
            d['usage'],
            d['side_effects'],
            d['dosage'],
            d['schedule'],
            d['notes'],
            d['history'],
            d['paused'],
            d['id'],
        )
#     def __init__(self, name, description, usage, side_effects, dosage, schedule: Schedule, notes='', history=None, paused=False, id=None):
    def __dict__(self):
        return {
            'name': self.name,
            'description': self.description,
            'missed_dose': self.missed_dose,
            'usage': self.usage,
            'side_effects': self.side_effects,
            'dosage': self.dosage,
            'schedule': self.schedule,
            'notes': self.notes,
            'history': self.history,
            'paused': self.paused,
            'id': self.id,
        }

    def get_schedule(self):
        return self.schedule
    
    def set_schedule(self, sched):
        self.schedule = sched

    def get_current_time(self):
        return self.schedule.get_current()

    def get_current_datetime(self):
        return self.schedule.get_current_datetime()

    def log_taken(self, late=False):
        self.__makelog__('applied',stuff={'late':late})

    def log_paused(self):
        self.__makelog__('paused medication')

    def log_resumed(self):
        self.__makelog__('resumed medication')

    def log_missed(self):
        self.__makelog__('missed')
        
    def log_dosechange(self):
        self.__makelog__('changed dosage')

    def __makelog__(self, type_, details='', stuff={}, timestamped=None):
        timestamp = timestamped if timestamped != None else time.time()
        t = self.get_schedule().repeat_time
        rstamp = math.floor(timestamp / t)*t
        out = stuff.copy()

        out['timestamp'] = timestamp
        out['name'] = self.name
        out['dosage'] = self.dosage
        out['description'] = type_
        out['details'] = details
        self.history['global'][rstamp] = self.history['global'].get(rstamp,[])
        self.history['global'][rstamp].append(out)

        self.history['categories'][type_] = self.history.get(type_,{})
        self.history['categories'][type_][rstamp] = self.history['categories'][type_].get(rstamp,[])
        self.history['categories'][type_][rstamp].append(out)

    def as_saveable(self):
        out = self.__dict__()
        out['schedule'] = out['schedule'].__dict__()
        return out

    def as_json(self):
        return json.dumps(self.as_saveable())


