#!/usr/bin/python3
import os
import sys
import math
import time
import datetime

from DataClasses import Schedule, MedicineData, RepeatSchedule
from SharedResources import font, timeU, timings
import SharedResources as SR

from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QHBoxLayout, QVBoxLayout, QMessageBox
# from PyQt5.QtGui import QSound
from PyQt5.QtMultimedia import QSound
from PyQt5.QtCore import QObject, QTimer






class MedicineListItem(QWidget):
    def __init__(self, delcall, editcall, medicineData, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.delcall = delcall
        self.editcall = editcall
        self.data = medicineData
        self.initUI()
    
    def togglePauseMed(self):
        if self.data.paused:
            self.unpauseMed()
        else:
            self.pauseMed()

    def pauseMed(self):
        self.data.setPaused(True)
        self.updateUI()
            

    def updatepause(self):
        if self.data.paused:
            self.BTN_pause.setText('⯈ Resume taking')
            self.setStyleSheet(SR.css.graylabel)
        else:
            self.BTN_pause.setText(' ⏸ Stop taking ')
            self.setStyleSheet('')
            self.data.setPaused(False)
            


    def unpauseMed(self):
        self.data.setPaused(False)
        self.updateUI()

    def PromptDelete(self):
        if self.deleteBox.exec() == QMessageBox.Discard:
            self.DeleteMe()

    def DeleteMe(self):
        self.delcall(self)

    def EditMe(self):
        self.editcall(self.data)

    def initUI(self):
        loadUi(SR.path.joinroot('ui','MedicineListItem.ui'), self)
        self.taken = False
        self.takentime = 0
        self.alertsound = QSound(SR.path.joinroot('sound','alert.wav'))

        self.deleteBox = QMessageBox(QMessageBox.Warning, 'Are you sure?', 'Are you sure you want to delete this medication?\n \nNote that it will no longer show up in your history. If you want to keep your history and not get alarted for dosages, press [⏸ Stop taking]', QMessageBox.Discard | QMessageBox.Cancel)
        self.alertBox = QMessageBox(QMessageBox.Information, 'Medication alert', f"It's time to apply {self.data.dosage} of {self.data.name}.")
        self.line_2.setStyleSheet(SR.css.linestyle)
        self.BTN_delete.clicked.connect(self.PromptDelete)

        self.BTN_edit.clicked.connect(self.EditMe)

        self.Title.setFont(font.h1)
        self.TimeLeft.setFont(font.h3)
        self.BTN_pause.clicked.connect(self.togglePauseMed)
        self.BTN_generic_accept.clicked.connect(self.signalTaken)

        self.timer = QTimer()
        self.timer.timeout.connect(self.updateUI)
        self.updateUI()
        self.timer.start(timings.timeupdate)

    def signalTaken(self, *args, **kwargs):
        self.data.log_taken()
        self.taken = True
        self.takentime = str(datetime.datetime.fromtimestamp(math.floor(time.time())))
        self.updateUI()

    def updateUI(self):
        self.updatepause()
        self.Title.setText(self.data.name)
        self.Dosage.setText(self.data.dosage)
        self.LBL_usage.setText(self.data.usage)
        self.LBL_description.setText(self.data.description)
        self.LBL_notes.setText(self.data.notes)
        self.LBL_side_effects.setText(self.data.side_effects)
        self.LBL_missed_dose.setText(self.data.missed_dose)
        if self.data.paused:
            self.BTN_generic_accept.setDisabled(True)
            self.LBL_TimeLeft.setText('No dosage scheduled')
            self.TimeLeft.setText('This medication \nwas put on hold')
            self.TimeLeft.setStyleSheet(SR.css.graylabel)
        else:
            curtime = self.data.get_current_datetime()
            cdt = curtime - datetime.datetime.now()
            timestr = []
            t = cdt.total_seconds()
            if t > 0:
                self.BTN_generic_accept.setDisabled(True)
                self.LBL_TimeLeft.setText('Next dosage due in:')
                [ timestr.append(f'{val} {name if val > 1 else name[:-1]}') for name, val in SR.func.delta_details(cdt).items() if val != 0 or name in ['minutes','seconds'] ]
                self.TimeLeft.setText('\n'.join(timestr))
                self.TimeLeft.setStyleSheet(SR.css.empty)
            else:
                if math.ceil(t) == 0:
                    self.taken = False
                    print(f'{self.data.name} ({self.data.id}) due now')
                    self.alertsound.play()
                    ret = self.alertBox.exec()
                reptime = self.data.get_schedule().repeat_time
                if self.taken:
                    self.BTN_generic_accept.setDisabled(True)
                    self.LBL_TimeLeft.setText('Applied on:')
                    # guy = self.data.history['categories']['applied'][math.floor(time.time() / reptime)*reptime]
                    self.TimeLeft.setText(self.takentime)
                    self.TimeLeft.setStyleSheet(SR.css.greenlabel)
                else:
                    self.TimeLeft.setStyleSheet(SR.css.salmonlabel)
                    self.BTN_generic_accept.setDisabled(False)
                    # print('ret:',ret)
                    self.LBL_TimeLeft.setText('Take within:')
                    within = datetime.timedelta(seconds=self.data.get_schedule().grace_time + cdt.total_seconds())
                    [ timestr.append(f'{val} {name if val > 1 else name[:-1]}') for name, val in SR.func.delta_details(within).items() if val != 0 or name in ['minutes','seconds'] ]
                    self.TimeLeft.setText('\n'.join(timestr))
            # print('curr', curtime.timestamp(),'\nnow', time.time())
            # print(cdt.total_seconds())
            # print(self.data.schedule.grace_time)
            # print()


if __name__ == '__main__':
    pass
    # import time
    # app = QApplication(sys.argv)



    # dummydata1 = MedicineData("Estrogen", "gay juice", 'slap that bitch on', 'a little bit of cancer', 'some 3mg', RepeatSchedule(timeU.days(3.5), 1632124800+timeU.days(2)))
    # widget1 = MedicineListItem(dummydata1)
    
    # dummydata2 = MedicineData("Gamer stab", "Very epic", 'stab', 'idk lol', 'who knows!!', RepeatSchedule(timeU.weeks(4), 1632124800), 'these are notes!!')
    # widget2 = MedicineListItem(dummydata2)
    

    # container = QWidget()
    # layout = QVBoxLayout()

    # layout.addWidget(widget1)
    # layout.addWidget(widget2)

    # container.setLayout(layout)

    # window = QMainWindow()
    # window.setCentralWidget(container)
    # window.show()
    # app.exec()
