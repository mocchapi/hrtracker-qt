from PyQt5.uic import loadUi
from PyQt5.QtWidgets import QMainWindow, QWidget
import SharedResources as SR
import os

class ExpandBox(QWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.initUI()
    
    def initUI(self):
        loadUi(os.path.join('ui',SR.path.root,'ExpandBox.ui'), self)
        self.Text.setVisible(False)
    
    def setTitle(self, newtit):
        self.Title.setText(newtit)
    
    def setContent(self, newtext):
        self.Text.setText(newtext)
