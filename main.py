from PyQt5.uic import loadUi
from PyQt5.QtGui import QIcon, QMovie, QPixmap
from PyQt5.QtCore import QObject, QTimer, Qt
from PyQt5.QtWidgets import QMainWindow, QWidget, QApplication, QStyle, QLabel, QMessageBox, QSplashScreen

import sys
import time

import DataClasses
import SharedResources as SR
from MedicineListItem import MedicineListItem
from EditForm import EditForm

class MainWidget(QWidget):
    def __init__(self, vault, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.vault = vault
        self.guys = {}
        self.initUI()
        self.initEditPage()

    def initUI(self): 
        loadUi(SR.path.joinroot('ui','MainWidget.ui'), self)
        for data in self.vault.get_all().values():
            # print('making',data)
            self.addListItem(data)
        self.NewButton.clicked.connect(self.summonEditPageNew)
    
    def closeEvent(self, event=None):
        self.vault.save()
        if event != None: event.accept()
    
    def addListItem(self, data):
        wid = MedicineListItem(self.removeListItem, self.summonEditPageEdit, data)
        self.MedicineList.insertWidget(0,wid)
        self.guys[data.id] = wid
        # print(f'Added {data.name} to the list')
    
    def removeListItem(self, item):
        med = item.data
        self.vault.remove(med)
        item.deleteLater()
        self.vault.save()

    def initEditPage(self):
        self.editWindow = EditForm(self.vault)
    
    def summonEditPageNew(self):
        self.editWindow.showAndCreate(self.createNewMedicineCallback)

    def summonEditPageEdit(self, data):
        self.editWindow.showAndEdit(data, self.editNewMedicineCallback)

    def editNewMedicineCallback(self,data):
        self.guys[data.id].updateUI()
        self.vault.save()

    def createNewMedicineCallback(self, data):
        self.vault.add(data)
        self.addListItem(data)
        self.vault.save()


def main(arg):
    app = QApplication(sys.argv)

    db = DataClasses.Vault(SR.path.joinroot('db.json'))
    # print('boop')
    widget = MainWidget(db)
    # print('bop')

    # widget.addListItem(DataClasses.MedicineData("Estradiol", "HRT", 'Put on a hairless spot', 'Weight gains, emotions', '100 nanogram/24h', DataClasses.RepeatSchedule(SR.timeU.days(3.5), 1632124800+SR.timeU.days(2))))
    # widget.addListItem(DataClasses.MedicineData("test", "test", 'test', 'tesdt', 'test', DataClasses.RepeatSchedule(SR.timeU.days(3.5), time.time()-SR.timeU.seconds(-3))))
    
    window = QMainWindow()
    window.setCentralWidget(widget)
    window.setWindowIcon(QIcon(SR.path.joinroot('img','SmallIconAlt.png')))
    window.setWindowTitle('HRTracker')
    window.resize(window.width(), 500)
    window.show()

    # splash_pix = QPixmap(SR.path.joinroot('SplashScreenSmall.png'))
    # splash = QSplashScreen(splash_pix, Qt.WindowStaysOnTopHint)
    # splash.show()

    # def discardSplash():
    #     splash.close()
    #     window.show()

    
    # QTimer.singleShot(2500, discardSplash)
    app.aboutToQuit.connect(widget.closeEvent)
    sys.exit(app.exec())

if __name__ == '__main__':
    main(sys.argv)